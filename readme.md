oddlydrawn's junk from learning python
=========================================================

This repo currently contains the following Python3 programs:

#### merge_sort.py
Tries to mergesort. A terrible interpretation of the gif on
[Wikipedia's page about mergesorting](https://en.wikipedia.org/wiki/Merge_sort)

#### pi_counter.py
Takes a positive integer as an argument and prints pi to the number of decimal
places specified by that argument, up to 1000 decimal places. Uses the
[Bailey–Borwein–Plouffe formula](https://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula).
Example usage:

        $ ./pi_counter.py 5
        3.14159

#### python_2048.py
A very simple implementation of 2048 in python. No score, No limits on number
of times numbers can be merged in a single turn (An entire column/row can be
merged with a single keystroke)

    Uses readchar, can be installed with pip:
        $ pip3 install --user readchar

    or system-wide with:
        # pip3 install readchar

