#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: merge_sort.py
Author: /u/oddlydrawn
Created: 2016-02-14
Description: Tries to mergesort
"""

import random

ARRAY_LENGTH = 11
MIN_NUM = -100
MAX_NUM = 100


def main():
    # unsorted_list = [7, 3, 2, 1, 8, 9, 10, 7, 1, 10, 100, 3, 8, 1]
    unsorted_list = random_list()
    print("unsorted_list: ", unsorted_list)

    sorted_list = []
    sorted_list = merge_sort(unsorted_list)

    sorted_list = sum(sorted_list, [])  # fixes list in list
    print("sorted_list:   ", sorted_list)

    python_sorted = sorted(unsorted_list)
    print("python_sorted: ", python_sorted)


def random_list():
    l = []
    for x in range(0, ARRAY_LENGTH):
        r = random.randint(MIN_NUM, MAX_NUM)
        l.append(r)

    return l


def merge_sort(unsorted_list):
    unsorted_list = split_list(unsorted_list)

    u_len = len(unsorted_list)

    while u_len > 1:
        merged_elements = []
        for index_one in range(0, u_len, 2):
            if last_element_has_no_pair(u_len, index_one):
                last_element = unsorted_list[-1]
                merged_elements.append(last_element)
                break

            index_two = index_one + 1
            list_one = unsorted_list[index_one]
            list_two = unsorted_list[index_two]

            combined_lists = merge_two_lists(list_one, list_two)
            merged_elements.append(combined_lists)

        unsorted_list = merged_elements
        u_len = len(unsorted_list)

    sorted_list = []
    sorted_list = unsorted_list

    return sorted_list


def split_list(unsplit_list):
    split = []
    for x in unsplit_list:
        split.append([x])

    return split


def last_element_has_no_pair(u_len, index_one):
    return is_odd(u_len) and index_one == u_len - 1


def is_odd(num):
    return num % 2 is not 0


def merge_two_lists(list_one, list_two):
    merged_list = []
    len_one = len(list_one)
    len_two = len(list_two)

    a = 0
    b = 0
    while a < len_one or b < len_two:
        if a == len_one:
            # ran out of list_one numbers, just add list_two number
            num_two = list_two[b]
            merged_list.append(num_two)
            b += 1
            continue

        elif b == len_two:
            # ran out of list_two numbers, just add list_one number
            num_one = list_one[a]
            merged_list.append(num_one)
            a += 1
            continue

        num_one = list_one[a]
        num_two = list_two[b]

        if num_one < num_two:
            merged_list.append(num_one)
            a += 1
        else:
            merged_list.append(num_two)
            b += 1

    return merged_list


if __name__ == "__main__":
    main()
