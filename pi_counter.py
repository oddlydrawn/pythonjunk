#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: pi_counter.py
Author: u/oddlydrawn
Created: 2016-02-20
Description: Takes a positive integer as an argument and prints pi to the
    number of decimal places specified by that argument, up to 1000 decimal
    places. Uses the Bailey–Borwein–Plouffe formula. Example usage:

        $ ./pi_counter.py 5
        3.14159
"""
import sys
from decimal import *
from mpmath import mp

DEBUG = True
# mpmath rounds, except for 1000th place
ROUND_RESULT = True


def main():
    if passes_input_validation():
        digits = int(sys.argv[1]) + 1  # 3 is first digit, need place
        if (digits > 1000):
            digits = 1000

        pi = str(get_pi_bbp(digits))
        print(pi)

        debug_pi(pi, digits)


def passes_input_validation():
    if len(sys.argv) is not 2:
        print_usage()
        return False

    input = sys.argv[1]
    if input.isdigit() is False:
        print("Please enter a whole, positive number")
        return False

    return True


def print_usage():
    print("Usage: py_counter.py [Number]")


def get_pi_bbp(digits):
    getcontext().prec = digits + 2  # Extra place for rounding

    base = Decimal(16)
    eight = Decimal(8)
    pi = Decimal(0)

    one = Decimal(1)
    two = Decimal(2)
    four = Decimal(4)
    five = Decimal(5)
    six = Decimal(6)

    for x in range(0, digits):
        k = Decimal(x)
        base_k = Decimal(base**Decimal(k))
        eight_k = Decimal(eight * k)

        a = Decimal(four / (base_k * (eight_k + one)))
        b = Decimal(two / (base_k * (eight_k + four)))
        c = Decimal(one / (base_k * (eight_k + five)))
        d = Decimal(one / (base_k * (eight_k + six)))

        pi += Decimal(a - b - c - d)

    getcontext().rounding = ROUND_HALF_UP
    getcontext().prec = digits

    if ROUND_RESULT:
        return +pi
    else:
        pi_str = str(pi)[:-2]
        return pi_str


def debug_pi(pi, digits):
    if DEBUG:
        pi_mp = str(get_pi_mpmath(digits))
        print(pi_mp)
        if pi == pi_mp:
            print("both pis are equal")


def get_pi_mpmath(digits):
    mp.dps = digits
    return mp.pi


if __name__ == "__main__":
    main()
