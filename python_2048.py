#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: python_2048.py
Author: u/oddlydrawn
Created: 2016-02-29
Description: A very simple implementation of 2048 in python. No score, No
limits on number of times numbers can be merged in a single turn (An
entire column/row can be merged with a single keystroke)

    Uses readchar, can be installed with pip:
        $ pip install --user readchar

    or system-wide with:
        # pip install readchar

"""

import random
import readchar

UP = '\x1b[A'
DOWN = '\x1b[B'
RIGHT = '\x1b[C'
LEFT = '\x1b[D'
QUIT = 'q'
game_on = True

str_lst = []
num_lst = []


def main():
    global str_lst
    global num_lst

    str_lst = new_2D_list(4, 4)
    num_lst = new_2D_list(4, 4)

    run_game()


def new_2D_list(x, y):
    return [[0 for a in range(x)] for b in range(y)]


def run_game():
    while game_on:
        add_new_piece()
        copy_number_to_string()
        print_board()
        print(num_lst)
        get_input()


def add_new_piece():
    global num_lst
    finding_empty_space = True

    while finding_empty_space:
        randX = random.randint(0, 3)
        randY = random.randint(0, 3)

        if num_lst[randX][randY] is 0:
            num_lst[randX][randY] = 2
            finding_empty_space = False


def copy_number_to_string():
    global str_lst

    for y in range(4):
        for x in range(4):
            if num_lst[x][y] is 0:
                str_lst[x][y] = "".center(6, ' ')
            else:
                str_lst[x][y] = str(num_lst[x][y]).center(6, ' ')


def print_board():
    print("""
          ┌──────┬──────┬──────┬──────┐
          │      │      │      │      │
          │{:s}│{:s}│{:s}│{:s}│
          │      │      │      │      │
          ├──────┼──────┼──────┼──────┤
          │      │      │      │      │
          │{:s}│{:s}│{:s}│{:s}│
          │      │      │      │      │
          ├──────┼──────┼──────┼──────┤
          │      │      │      │      │
          │{:s}│{:s}│{:s}│{:s}│
          │      │      │      │      │
          ├──────┼──────┼──────┼──────┤
          │      │      │      │      │
          │{:s}│{:s}│{:s}│{:s}│
          │      │      │      │      │
          └──────┴──────┴──────┴──────┘

          """.format(
              str_lst[0][0], str_lst[1][0], str_lst[2][0], str_lst[3][0],
              str_lst[0][1], str_lst[1][1], str_lst[2][1], str_lst[3][1],
              str_lst[0][2], str_lst[1][2], str_lst[2][2], str_lst[3][2],
              str_lst[0][3], str_lst[1][3], str_lst[2][3], str_lst[3][3]))


def get_input():
    print("Arrows to move, '{}' to quit".format(QUIT))
    key = readchar.readkey()

    if key == UP and can_move(UP):
        move_up()
    elif key == DOWN and can_move(DOWN):
        move_down()
    elif key == RIGHT and can_move(RIGHT):
        move_right()
    elif key == LEFT and can_move(LEFT):
        move_left()
    elif key == QUIT:
        global game_on
        game_on = False
    else:
        get_input()


def can_move(d):
    if d == UP:
        for y in range(1, 4):
            for x in range(4):
                if not_empty(x, y) and dest_is_empty(x, y - 1):
                    return True
                elif not_empty(x, y) and can_combine(x, y, x, y - 1):
                    return True

    if d == DOWN:
        for y in range(2, -1, -1):
            for x in range(4):
                if not_empty(x, y) and dest_is_empty(x, y + 1):
                    return True
                elif not_empty(x, y) and can_combine(x, y, x, y + 1):
                    return True

    if d == RIGHT:
        for y in range(4):
            for x in range(2, -1, -1):
                if not_empty(x, y) and dest_is_empty(x + 1, y):
                    return True
                elif not_empty(x, y) and can_combine(x, y, x + 1, y):
                    return True

    if d == LEFT:
        for y in range(4):
            for x in range(1, 4):
                if not_empty(x, y) and dest_is_empty(x - 1, y):
                    return True
                elif not_empty(x, y) and can_combine(x, y, x - 1, y):
                    return True


def move_up():
    for y in range(1, 4):
        for x in range(4):
            from_x = x
            from_y = y
            while from_y > 0:
                to_x = x
                to_y = from_y - 1

                handle_movement(from_x, from_y, to_x, to_y)
                from_y -= 1


def move_down():
    for y in range(2, -1, -1):
        for x in range(4):
            from_x = x
            from_y = y
            while from_y < 3:
                to_x = x
                to_y = from_y + 1

                handle_movement(from_x, from_y, to_x, to_y)
                from_y += 1


def move_right():
    for y in range(4):
        for x in range(2, -1, -1):
            from_x = x
            from_y = y
            while from_x < 3:
                to_x = from_x + 1
                to_y = y

                handle_movement(from_x, from_y, to_x, to_y)
                from_x += 1


def move_left():
    for y in range(4):
        for x in range(1, 4):
            from_x = x
            from_y = y
            while from_x > 0:
                to_x = from_x - 1
                to_y = y

                handle_movement(from_x, from_y, to_x, to_y)
                from_x -= 1


def handle_movement(from_x, from_y, to_x, to_y):
    if not_empty(from_x, from_y):
        if dest_is_empty(to_x, to_y):
            print_move(from_x, from_y, to_x, to_y)
            move_num(from_x, from_y, to_x, to_y)

        elif can_combine(from_x, from_y, to_x, to_y):
            print_move(from_x, from_y, to_x, to_y)
            combine_num(from_x, from_y, to_x, to_y)


def not_empty(from_x, from_y):
    return num_lst[from_x][from_y] is not 0


def dest_is_empty(to_x, to_y):
    return num_lst[to_x][to_y] is 0


def can_combine(from_x, from_y, to_x, to_y):
    return num_lst[to_x][to_y] == num_lst[from_x][from_y]


def print_move(from_x, from_y, to_x, to_y):
    line = "moving ({}, {}) = {}".format(
        from_x, from_y, num_lst[from_x][from_y])
    line += "\tto ({}, {}) = {}".format(
        to_x, to_y, num_lst[to_x][to_y])
    print(line)


def move_num(from_x, from_y, to_x, to_y):
    num_lst[to_x][to_y] = num_lst[from_x][from_y]
    num_lst[from_x][from_y] = 0


def combine_num(from_x, from_y, to_x, to_y):
    num = num_lst[from_x][from_y] + num_lst[to_x][to_y]
    num_lst[to_x][to_y] = num
    num_lst[from_x][from_y] = 0


if __name__ == "__main__":
    main()
